package com.mon_test;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.widget.Button;

public class MainActivity2 extends AppCompatActivity {
    final String PAYS = "pays";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);


        final Button q2reponse1 = (Button) findViewById(R.id.buttonRoll);
        final Button q2reponse2 = (Button) findViewById(R.id.buttonRoll2);
        final Button q2reponse3 = (Button) findViewById(R.id.buttonRoll3);

        View.OnClickListener clickSurBouton1 = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent2 = new Intent(MainActivity2.this, resultat2.class);
                intent2.putExtra(PAYS, q2reponse1.getText().toString());
                startActivity(intent2);
            }
        };

        View.OnClickListener clickSurBouton2 = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent2 = new Intent(MainActivity2.this, resultat2.class);
                intent2.putExtra(PAYS, q2reponse2.getText().toString());
                startActivity(intent2);
            }
        };

        View.OnClickListener clickSurBouton3 = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent2 = new Intent(MainActivity2.this, resultat2.class);
                intent2.putExtra(PAYS, q2reponse3.getText().toString());
                startActivity(intent2);
            }
        };

        q2reponse1.setOnClickListener(clickSurBouton1);
        q2reponse2.setOnClickListener(clickSurBouton2);
        q2reponse3.setOnClickListener(clickSurBouton3);
    }
}