package com.mon_test;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity3 extends AppCompatActivity {

    final String MAIL = "mail";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        final Button q3reponse1 = (Button) findViewById(R.id.buttonRoll);
        final Button q3reponse2 = (Button) findViewById(R.id.buttonRoll2);
        final Button q3reponse3 = (Button) findViewById(R.id.buttonRoll3);

        View.OnClickListener clickSurBouton1 = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent3 = new Intent(MainActivity3.this, resultat3.class);
                intent3.putExtra(MAIL, q3reponse1.getText().toString());
                startActivity(intent3);
            }
        };

        View.OnClickListener clickSurBouton2 = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent3 = new Intent(MainActivity3.this, resultat3.class);
                intent3.putExtra(MAIL, q3reponse2.getText().toString());
                startActivity(intent3);
            }
        };

        View.OnClickListener clickSurBouton3 = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent3 = new Intent(MainActivity3.this, resultat3.class);
                intent3.putExtra(MAIL, q3reponse3.getText().toString());
                startActivity(intent3);
            }
        };

        q3reponse1.setOnClickListener(clickSurBouton1);
        q3reponse2.setOnClickListener(clickSurBouton2);
        q3reponse3.setOnClickListener(clickSurBouton3);
    }
}