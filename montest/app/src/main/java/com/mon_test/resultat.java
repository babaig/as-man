package com.mon_test;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class resultat extends AppCompatActivity {

    private Button buttonNext;
    final String VILLE = "ville";

    public int scorequizz;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultat);

        Intent intent1 = getIntent();
        String villeChoisie = intent1.getStringExtra(VILLE);

        String resultat = "Mauvaise réponse";
        if(villeChoisie.toLowerCase().equals("paris")){

            scorequizz++;
            resultat = "Bonne réponse" + scorequizz;

        }

        TextView vueResultat = (TextView) findViewById(R.id.textResultatQuestion);

        if(vueResultat != null){
            vueResultat.setText(resultat);

        }

        this.buttonNext = (Button) findViewById(R.id.buttonNext);
        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent otherActivity = new Intent(getApplicationContext(), MainActivity2.class);
                startActivity(otherActivity);
                finish();

            }
        });
    }
}
