package com.mon_test;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class resultat2 extends AppCompatActivity {

    private Button buttonNext;

    final String PAYS = "pays";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultat2);

        Intent intent2 = getIntent();
        String paysChoisie = intent2.getStringExtra(PAYS);

        String resultat = "Mauvaise réponse";
        if(paysChoisie.toLowerCase().equals("266")){
            resultat = "Bonne réponse";
        }

        TextView vueResultat = (TextView) findViewById(R.id.textResultatQuestion);

        if(vueResultat != null){
            vueResultat.setText(resultat);
        }

        this.buttonNext = (Button) findViewById(R.id.buttonNext);

        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent otherActivity = new Intent(getApplicationContext(), MainActivity3.class);
                startActivity(otherActivity);
                finish();
            }
        });
    }
}