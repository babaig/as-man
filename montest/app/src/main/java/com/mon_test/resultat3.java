package com.mon_test;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class resultat3 extends AppCompatActivity {

    private Button buttonNext;
    final String MAIL = "mail";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultat3);

        Intent intent3 = getIntent();
        String nbmail = intent3.getStringExtra(MAIL);

        String resultat = "Mauvaise réponse";
        if(nbmail.toLowerCase().equals("250 000")){
            resultat = "Bonne réponse";
        }

        TextView vueResultat = (TextView) findViewById(R.id.textResultatQuestion);

        if(vueResultat != null){
            vueResultat.setText(resultat);

        }

        this.buttonNext = (Button) findViewById(R.id.buttonNext);
        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent otherActivity = new Intent(getApplicationContext(), scorequizz.class);
                startActivity(otherActivity);
                finish();

            }
        });
    }
}